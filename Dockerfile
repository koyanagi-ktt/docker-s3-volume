FROM alpine:3.6
MAINTAINER Kazuma Koyanagi <koyanagi@kttnet.co.jp>

RUN apk --no-cache add bash py-pip inotify-tools && pip install awscli
ADD watch /watch

VOLUME /data

ENTRYPOINT [ "./watch" ]
CMD ["/data"]
